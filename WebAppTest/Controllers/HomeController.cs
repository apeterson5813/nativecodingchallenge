﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebAppTest.Controllers
{
    public class HomeController : Controller
    {
        public RedirectResult Index()
        {
            return RedirectPermanent("/Help");
        }
    }
}
