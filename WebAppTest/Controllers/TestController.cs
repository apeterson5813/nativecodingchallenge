﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebAppTest.Controllers
{
    public class TestController : ApiController
    {
        public IHttpActionResult Get(string simulateResponse = "200")
        {
            switch (simulateResponse)
            {
                case "200":
                    var now = DateTime.Now.ToString("s");
                    return Ok(now);
                default:
                    return InternalServerError();
            }
        }
    }
}
