﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ThreadingTest
{
    class Program
    {
        // Input list of numbers
        static List<int> inputList = new List<int> { 1, 2, 3, 4, 5 };
        // URL to print out the results of a GET request
        static string testUri = "https://www.example.com";
        // Create the HttpClient here so that the entire program has access to the same HttpClient 
        private static HttpClient Client = new HttpClient();

        static int SumEvens(List<int> numberList)
        {
            int sum = 0;

            // Using a for loop (instaed of 'foreach' or List.Where)  because it does not involve an 
            // IEnumerator and the associated overhead. 
            for (int i = 0; i < numberList.Count(); i++)
            {
                if (numberList[i] % 2 == 0)
                {
                    sum += numberList[i];
                }
            }

            return sum;

        }

        static async Task<string> Get(string uri)
        {
            return await Client.GetStringAsync(uri);
        }

        static void DisplayGet(string uri)
        {
            string getResponse = Get(uri).Result;
            Console.WriteLine(getResponse);
        }

        static void PrintNumbersOnDelay(List<int> numberList, int delay)
        {
            string threadName = Thread.CurrentThread.Name;
            for (int i = 0; i < numberList.Count(); i++)
            {
                Thread.Sleep(delay);
                Console.WriteLine($"{threadName}: {numberList[i]}");
            }
        }



        static void Main(string[] args)
        {
            int result = SumEvens(inputList);
            Console.WriteLine(string.Format($"The sum of all the even numbers is: {result}", result));

            DisplayGet(testUri);

            Thread t1 = new Thread(()=>PrintNumbersOnDelay(inputList, 500));

            List<int> reversedList = inputList.ToList();
            reversedList.Reverse();
            Thread t2 = new Thread(()=>PrintNumbersOnDelay(reversedList, 1000));

            t1.Name = "t1";
            t2.Name = "t2";

            t1.Start();
            t2.Start();

            t1.Join();
            t2.Join();

        }
    }
}
